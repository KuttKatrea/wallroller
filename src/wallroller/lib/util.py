import random
from typing import Iterable

secrets = random.SystemRandom()


def mklist(item):
    if not isinstance(item, Iterable):
        return [item]
    if isinstance(item, str):
        return [item]
    return item


def flatten(lst):
    if lst is None:
        return []
    lstoflst = [mklist(sublist) for sublist in lst]
    return [item for sublist in lstoflst for item in sublist]


class RetryCounter:
    def __init__(self, max_retries: int):
        self.max_retries = max_retries
        self.left_retries = max_retries

    def consume(self):
        self.left_retries -= 1

    def consumed(self):
        return self.max_retries - self.left_retries

    def available(self):
        return self.left_retries

    def has_available(self):
        return self.left_retries > 0
