import datetime
import logging
import os
import os.path
import sqlite3

from wallroller.lib.exception import ControlledException

USERAGENT = (
    "Mozilla/5.0 "
    "(Windows NT 10.0; Win64; x64; rv:93.0) "
    "Gecko/20100101 Firefox/93.0"
)


class EmptyGalleryException(ControlledException):
    pass


class BooruCategoryGroup:
    def __init__(self, tags: str):
        self.tags: str = tags


class BooruCache:
    def __init__(self, cache_dir):
        os.makedirs(cache_dir, exist_ok=True)
        self.cachefile = os.path.join(cache_dir, "booru-cache.sqlite")
        self.initialized_ = False

    def initialize(self):
        if self.initialized_:
            return

        self.initialized_ = True

        conn = self.get_connection_()

        cur = conn.cursor()
        cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='booru_maxpage_cache'"
        )
        result = cur.fetchone()
        if result is None:
            conn.execute(
                """CREATE TABLE booru_maxpage_cache(url text, tags text, max_page int)"""
            )

        cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='booru_post_cache'"
        )
        result = cur.fetchone()
        if result is None:
            conn.execute(
                (
                    "CREATE TABLE booru_post_cache("
                    "post_url text, image_url text, filename text, "
                    "search_tags text, source_tags text, updated_at text)"
                )
            )

        conn.close()

    def get_connection_(self):
        return sqlite3.connect(self.cachefile)

    def get_maxpage(self, url, tags):
        conn = self.get_connection_()
        c = conn.cursor()
        c.execute(
            """SELECT max_page FROM booru_maxpage_cache WHERE url=? AND tags=?""",
            (url, tags),
        )
        record = c.fetchone()
        conn.close()
        if record is None:
            return 1

        return record[0]

    def update_maxpage(self, url, tags, maxpage):
        logging.debug(
            "Updating maxpage for url %s and tags %s to %s", url, tags, maxpage
        )
        conn = self.get_connection_()
        c = conn.cursor()
        c.execute(
            """SELECT COUNT(*) FROM booru_maxpage_cache WHERE url=? AND tags=?""",
            (url, tags),
        )
        result = c.fetchone()

        if result[0] > 0:
            c.execute(
                """UPDATE booru_maxpage_cache SET max_page=? WHERE url=? AND tags=?""",
                (maxpage, url, tags),
            )
        else:
            c.execute(
                """INSERT INTO booru_maxpage_cache VALUES (?,?,?)""",
                (url, tags, maxpage),
            )

        conn.commit()
        conn.close()

    def update_post(self, post_url, image_url, filename, search_tags, source_tags):
        updated_at = datetime.datetime.now().isoformat()

        conn = self.get_connection_()
        c = conn.cursor()
        c.execute(
            """SELECT COUNT(*) FROM booru_post_cache WHERE post_url=?""", (post_url,)
        )
        result = c.fetchone()

        if result[0] > 0:
            c.execute(
                "UPDATE booru_post_cache "
                "SET image_url=?, filename=?, search_tags=?, source_tags=?, updated_at=? "
                "WHERE post_url=?",
                (
                    image_url,
                    filename,
                    search_tags,
                    " ".join(source_tags),
                    updated_at,
                    post_url,
                ),
            )
        else:
            c.execute(
                """INSERT INTO booru_post_cache VALUES (?,?,?,?,?,?)""",
                (
                    post_url,
                    image_url,
                    filename,
                    search_tags,
                    " ".join(source_tags),
                    updated_at,
                ),
            )

        conn.commit()
        conn.close()


cache_instances: dict[str, BooruCache] = {}


def get_cache(cache_dir):
    instance = cache_instances.get(cache_dir)
    if instance is None:
        instance = BooruCache(cache_dir)
        instance.initialize()
        cache_instances[cache_dir] = instance

    return instance


def group_hint_matcher(hint):
    def group_matches_hint(group: BooruCategoryGroup):
        return hint in group.tags

    return group_matches_hint
