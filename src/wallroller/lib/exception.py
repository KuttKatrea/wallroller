class ControlledException(Exception):
    pass


class InvalidProviderError(Exception):
    pass


class NoItemException(ControlledException):
    pass


class ItemNotNewException(ControlledException):
    pass


class InvalidItemException(ControlledException):
    pass


class BackoffException(ControlledException):
    pass
