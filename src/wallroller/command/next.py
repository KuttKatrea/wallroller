import click

from wallroller.command.common import pass_manager
from wallroller.config import SelectionOptions
from wallroller.manager import Manager


@pass_manager
def default_retries(manager):
    return manager.config.retries


@pass_manager
def default_new_only(manager):
    return manager.config.newonly


@click.command()
@click.option("--hint", type=click.STRING, default=None, required=False)
@click.option("--provider", type=click.STRING, default=None, required=False)
@click.option("--retries", type=click.INT, default=default_retries)
@click.option("--new-only", is_flag=True, default=default_new_only)
@click.option("--download-only", is_flag=True, default=False)
@click.option("--known-pages-only", is_flag=True, default=False)
@pass_manager
def command_next(
    manager: Manager,
    hint: str | None,
    provider: str | None,
    retries: int,
    new_only: bool,
    download_only: bool,
    known_pages_only: bool,
):
    options = SelectionOptions(
        new_only=new_only,
        download_only=download_only,
        known_pages_only=known_pages_only,
        hint=hint,
        selected_providers=parse_list(provider),
        retries=retries,
    )
    manager.wallpaper_change(options=options)


def parse_list(source: str | None):
    if not source:
        return []
    return source.split(",")
