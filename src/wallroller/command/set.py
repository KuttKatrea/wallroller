import click

from wallroller.command.common import pass_manager
from wallroller.image import Image
from wallroller.manager import Manager


@click.command()
@click.argument("file", type=click.Path(exists=True, dir_okay=False, resolve_path=True))
@pass_manager
def command_set(manager: Manager, file: str):
    selected = Image(
        file,
    )

    manager.wallpaper_apply(selected)
