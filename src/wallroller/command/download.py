import logging
import os.path

import click

from wallroller import downloader
from wallroller.command.common import pass_manager
from wallroller.image import Image
from wallroller.manager import Manager


@click.command("download")
@click.argument("url")
@click.option(
    "--output-path",
    help="Path to store the downloaded file",
    type=click.Path(exists=False, dir_okay=False, writable=True, resolve_path=True),
    prompt="Outputh path",
)
@click.option("--apply/--no-apply", help="Apply the downloaded picture", default=False)
@pass_manager
def command_download(manager: Manager, url: str, output_path: str, apply: bool):
    """
    Download a wallpaper from a known url
    """

    if os.path.isabs(output_path):
        output_path = os.path.abspath(output_path)

    logging.debug("Source: %s", url)
    logging.debug("Outputh path: %s", output_path)

    downloader.download(url, output_path, url)
    if apply:
        manager.wallpaper_apply(Image(title=output_path, path=output_path, source=url))
