import json

import click
from rich.console import Console
from rich.table import Table

from wallroller.command.common import pass_manager
from wallroller.manager import Manager


@click.command()
@pass_manager
def command_config(manager: Manager):
    table = Table()

    table.add_column("Provider")
    table.add_column("Engine")
    table.add_column("Config")

    for key, value in manager.providers.providers_.items():
        table.add_row(key, value.engine_id, json.dumps(value.config))

    console = Console()
    console.print(table)
