import json
import logging
import os.path
from collections.abc import Iterable
from typing import Mapping, NamedTuple, cast

import yaml
from appdirs import AppDirs

APPNAME = "wallroller"
PROFILES_DIR = "profiles"

local_dirs = AppDirs("wallroller", appauthor=False)
roaming_dirs = AppDirs("wallroller", appauthor=False, roaming=True)
config_dir = roaming_dirs.user_config_dir


def get_config_path(*args) -> str:
    return str(os.path.join(config_dir, *args))


def get_profile_config_file(profile) -> str:
    return get_config_path(PROFILES_DIR, f"{profile}.yaml")


def ensure_dir_exists_(dir_path) -> str:
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    return dir_path


class Config:
    def __init__(
        self,
        profile: str,
        config: dict,
        profile_working_dir: str,
        providers_dir: str,
        profile_providers_dir: str,
    ):
        self.profile = profile
        self.timeout: int | None = None
        self.newonly: bool = False

        self.config = config
        self.profile_working_dir = profile_working_dir
        self.providers_dir = providers_dir
        self.profile_providers_dir = profile_providers_dir

        self.retries = self.config.get("retries", 1)

    @property
    def providers(self) -> Mapping:
        return cast(Mapping, self.config.get("providers"))

    def get_provider_config_path(self, provider):
        return ensure_dir_exists_(os.path.join(self.providers_dir, provider))

    def get_provider_profile_config_path(self, provider):
        return ensure_dir_exists_(os.path.join(self.profile_providers_dir, provider))


def load_yaml_lines(filename: str):
    with open(filename, encoding="utf-8") as fptr:
        content = fptr.readlines()

    i = 0
    while i < len(content):
        line = content[i]
        if line.startswith("#include "):
            include_path = line[9:].strip()
            if not os.path.isabs(include_path):
                include_path = os.path.abspath(
                    os.path.join(os.path.dirname(filename), include_path)
                )

            subcontent = load_yaml_lines(include_path)

            j = 0
            while j < len(subcontent):
                content.insert(i + j, subcontent[j])
                j += 1
            i += len(subcontent)
        i += 1

    return content


def replace_variables(source, variables):
    for k in variables.keys():
        source = source.replace(f"<{k}>", variables[k])

    return source


def apply_variables(config, variables):
    if isinstance(config, dict):
        processed_config = {}
        for k in config.keys():
            processed_config[k] = apply_variables(config[k], variables)
        return processed_config

    if isinstance(config, list):
        return [apply_variables(config_item, variables) for config_item in config]

    if isinstance(config, str):
        return replace_variables(config, variables)

    return config


def configure_for(profile: str, yaml_file: str) -> Config:
    data_dir = ensure_dir_exists_(get_config_path("data"))

    profile_working_dir = ensure_dir_exists_(os.path.join(data_dir, profile))

    providers_dir = ensure_dir_exists_(os.path.join(get_config_path("providers")))

    profile_providers_dir = ensure_dir_exists_(
        os.path.join(profile_working_dir, "providers")
    )

    logging.info("Loading configuration file at: %s", yaml_file)
    content = load_yaml_lines(yaml_file)

    config = yaml.load("".join(content), Loader=yaml.SafeLoader)

    config["providers"] = apply_variables(config["providers"], config["vars"])

    logging.debug(
        "Using configuration: %s", json.dumps(config, indent=2, separators=(",", ": "))
    )

    return Config(
        profile, config, profile_working_dir, providers_dir, profile_providers_dir
    )


class SelectionOptions(NamedTuple):
    new_only: bool
    download_only: bool
    known_pages_only: bool
    selected_providers: Iterable[str] | None
    hint: str | None
    retries: int | None
