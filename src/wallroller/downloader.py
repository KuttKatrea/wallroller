import logging
import math
import os
import os.path
import shutil
from typing import Mapping, NamedTuple

import requests

from wallroller.providers.base import create_metafile


class ColorTuple(NamedTuple):
    red: int
    green: int
    blue: int


class WatermarkColor(NamedTuple):
    fg_color: int | tuple[int, int, int] | tuple[int, int, int, int]
    bg_color: int | tuple[int, int, int] | tuple[int, int, int, int]


class DownloadResult(NamedTuple):
    is_downloaded: bool


def download(
    url, output_path, source_url, request_parameters=None, meta: Mapping | None = None
) -> DownloadResult:
    logging.debug("Creating output directories")
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    logging.info("Downloading %s to %s", url, output_path)

    create_metafile(output_path + ".url", source_url, meta)

    if os.path.exists(output_path):
        logging.info("Already exists, not downloading")
        return DownloadResult(is_downloaded=False)

    request_parameters = request_parameters or {}
    request_parameters.setdefault("headers", {})
    request_parameters["headers"].setdefault(
        "user-agent",
        "Mozilla/5.0 "
        "(Windows NT 10.0; Win64; x64; rv:93.0)"
        " Gecko/20100101 Firefox/93.0",
    )
    request_parameters["headers"].setdefault("referer", source_url)

    logging.debug("Using request params: %s", request_parameters)

    _, suffix = os.path.splitext(output_path)
    tmp_path = output_path + ".partial"

    logging.debug(f"Creating downloading to temporal file {tmp_path}")
    with open(tmp_path, "wb") as tmpfp, requests.get(
        url, stream=True, **request_parameters
    ) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8192):
            # If you have chunk encoded response uncomment if
            # and set chunk_size parameter to None.
            # if chunk:
            tmpfp.write(chunk)

    logging.debug(f"Renaming temporal file {tmp_path} to {output_path}")

    shutil.move(tmp_path, output_path)

    logging.debug("Download complete")
    return DownloadResult(is_downloaded=True)


def get_average_color_int(image, min_x, min_y, max_x, max_y) -> ColorTuple:
    """Returns a 3-tuple containing the RGB value of the average color of the
    given square bounded area of length = n whose origin (top left corner)
    is (x, y) in the given image"""

    rgb_im = image.convert("RGB")

    red, green, blue = 0, 0, 0
    count = 0
    for s in (min_x, max_x):
        for t in (min_y, max_y):
            # if (s <= min_x) or ( s >= max_x) or (t <= min_y) or (t >= max_y):
            pixlr, pixlg, pixlb = rgb_im.getpixel((s, t))
            red += pixlr
            green += pixlg
            blue += pixlb
            count += 1

    red = math.floor(red / count)
    green = math.floor(green / count)
    blue = math.floor(blue / count)
    return ColorTuple(red, green, blue)
