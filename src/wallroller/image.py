import os.path
from dataclasses import dataclass


@dataclass
class Image(object):
    path: str
    title: str | None = None
    source: str | None = None
    source_detail: str | None = None
    provider: str | None = None
    is_downloaded: bool = True

    def __post_init__(self):
        if self.source is None:
            self.source = self.path

        if self.title is None:
            self.title = os.path.basename(self.path)
