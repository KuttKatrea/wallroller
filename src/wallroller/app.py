import logging

import click
import click_logging

from wallroller import wallroller
from wallroller.command.config import command_config
from wallroller.command.download import command_download
from wallroller.command.next import command_next
from wallroller.command.set import command_set


@click.group()
@click_logging.simple_verbosity_option(logging.root)
@click.option("--profile", default="default")
@click.pass_context
def cli(ctx, profile):
    ctx.obj = wallroller.build(profile=profile)


def main():
    cli.add_command(command_config, name="config")
    cli.add_command(command_set, name="set")
    cli.add_command(command_download, name="download")
    cli.add_command(command_next, name="next")

    try:
        cli()
    except Exception as ex:
        logging.exception(ex)
