import platform
import sys

from .command import ManagerProtocol, PlatformHooksBase


def create_platform_hooks(manager: ManagerProtocol) -> PlatformHooksBase:
    os = platform.system()

    if os == "Windows":
        assert sys.platform == "win32"
        from . import win as current
    elif os == "Darwin":
        assert sys.platform == "darwin"
        from . import mac as current  # type: ignore
    else:
        assert sys.platform == "linux"
        from . import linux as current  # type: ignore

    return current.PlatformHooks(manager)
