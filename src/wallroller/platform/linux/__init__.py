import logging
import os
import sys

from .. import PlatformHooksBase
from . import wallprovider

assert sys.platform == "linux"


def notify(title, body, icon="nitrogen"):
    command = 'notify-send.sh --replace-file /tmp/wallroller -i "%s" "%s" "%s"' % (
        icon,
        title.replace('"', '\\"'),
        body.replace('"', '\\"'),
    )

    logging.info(command)
    os.system(command)


def set_wallpaper_file(path):
    wallprovider.set_wallpaper(path)


class PlatformHooks(PlatformHooksBase):
    def __init__(self, manager):
        super().__init__(manager)
        self.manager = manager
        self.config = self.manager.config

    def notify_changing(self):
        notify("Wallroller", "Selecting next wallpaper")

    def notify_change(self, selected):
        title = selected.provider if selected.provider else "Wallroller"

        pieces = [f'File: <a href="file://{selected.path}">{selected.title}</a>']

        if selected.source:
            pieces.append(
                "<i>Source: <a href='%s'>%s</a></i>%s"
                % (
                    selected.source,
                    selected.source,
                    (
                        " (" + selected.source_detail + ")"
                        if selected.source_detail
                        else ""
                    ),
                )
            )

        body = "\n".join(pieces)
        icon = selected.path

        notify(title, body, icon)

    def notify_error(self, err):
        notify("Wallroller", err, "nitrogen")

    def set_wallpaper(self, selected):
        set_wallpaper_file(selected.path)
