import logging
import math
import statistics
import subprocess
import sys

from PIL import Image

assert sys.platform == "linux"

DELTAPROP = 0.15
CENTERPROP = 0.75
TILEPROP = 0.0

PARAM_TILED = "--bg-tile"
PARAM_CENTERED = "--bg-center"
PARAM_FILL = "--bg-fill"
PARAM_FIT = "--bg-max"

FILLING_BARS_NONE = 0
FILLING_BARS_TOP_BOTTOM = 1
FILLING_BARS_LEFT_RIGHT = 2
FILLING_BARS_ALL = FILLING_BARS_TOP_BOTTOM | FILLING_BARS_LEFT_RIGHT


def set_wallpaper(filename):
    with Image.open(filename) as image:
        width, height = image.size

        proportion = width / height

        logging.info(
            "WALLPROVIDER: Image [ Size: %dx%d | Proportion: %f ]",
            width,
            height,
            proportion,
        )

        screen_width, screen_height = get_resolution()
        screen_proportion = screen_width / screen_height

        logging.info(
            "WALLPROVIDER: Screen [ Size: %dx%d | Proportion: %f ]",
            screen_width,
            screen_height,
            screen_proportion,
        )

        proportion_raw = screen_proportion - proportion
        proportion_delta = math.fabs(proportion_raw)
        proportion_perc = proportion_delta / screen_proportion

        logging.info(
            "WALLPROVIDER: Difference: %f / %f %%", proportion_delta, proportion_perc
        )

        filling_bars = FILLING_BARS_ALL

        if width < (CENTERPROP * screen_width) and height < (
            CENTERPROP * screen_height
        ):
            if width < (TILEPROP * screen_width):
                param = PARAM_TILED
            else:
                param = PARAM_CENTERED
        elif proportion_perc <= DELTAPROP:
            param = PARAM_FILL
        else:
            param = PARAM_FIT
            if proportion_raw < 0:
                filling_bars = FILLING_BARS_TOP_BOTTOM
            else:
                filling_bars = FILLING_BARS_LEFT_RIGHT

        color = get_average_color_int(image, filling_bars)

    logging.info("WALLPROVIDER: %s : %s : %s", color, param, filename)

    command = [
        "feh",
        f"--image-bg={hexcolor(color)}",
        param,
        filename,
    ]

    subprocess.run(command, check=True)


def get_randr_version():
    command = (
        r"xrandr -v | "
        r'egrep -o "Server reports RandR version [0-9]+\.[0-9]" | '
        r'cut -d" " -f 5'
    )
    res = subprocess.check_output(command, shell=True)
    res = res.decode(sys.stdout.encoding).strip()
    return res


def get_resolution():
    randrver = get_randr_version()
    logging.info("Using RandR version: %s", randrver)

    if randrver == "1.1":
        return get_resolution_1_1()

    if randrver in ("1.2", "1.3", "1.4", "1.5", "1.6"):
        return get_resolution_1_4()

    raise Exception("RandR version %s unsuported" % randrver)


def get_resolution_1_1():
    command = 'xrandr | egrep -o "*[0-9]\\s+[0-9]+ x [0-9]+" | cut -d" " -f 4,6'
    res = subprocess.check_output(command, shell=True)
    res = res.decode(sys.stdout.encoding).strip()
    w, h = res.split(" ")

    return (int(w), int(h))


def get_resolution_1_4():
    command = 'xrandr | egrep -o "current [0-9]+ x [0-9]+" | cut -d" " -f 2,4'
    res = subprocess.check_output(command, shell=True)
    res = res.decode(sys.stdout.encoding).strip()
    w, h = res.split(" ")

    return (int(w), int(h))


def get_average_color(image):
    return "#%x%x%x" % get_average_color_int(image)


def get_average_color_int(image, filling_bars=FILLING_BARS_NONE):
    """
    Calculate average color of the marquee of an image.

    Return a 3-tuple containing the RGB value of the average color of the
    given square bounded area of length = n whose origin (top left corner)
    is (x, y) in the given image
    """

    width, height = image.size

    rgb_im = image.convert("RGB")

    r, g, b = 0, 0, 0
    count = 0

    r_list = []
    g_list = []
    b_list = []

    if filling_bars & FILLING_BARS_TOP_BOTTOM:
        for s in (0,):
            for t in range(0, height):
                pixlr, pixlg, pixlb = rgb_im.getpixel((s, t))
                r += pixlr
                g += pixlg
                b += pixlb

                r_list.append(pixlr)
                g_list.append(pixlg)
                b_list.append(pixlb)

                count += 1

    if filling_bars & FILLING_BARS_LEFT_RIGHT:
        for s in range(0, width):
            for t in (height - 1,):
                pixlr, pixlg, pixlb = rgb_im.getpixel((s, t))
                r += pixlr
                g += pixlg
                b += pixlb

                r_list.append(pixlr)
                g_list.append(pixlg)
                b_list.append(pixlb)

                count += 1

    return (
        math.floor(statistics.median_grouped(r_list)),
        math.floor(statistics.median_grouped(g_list)),
        math.floor(statistics.median_grouped(b_list)),
    )


def hexcolor(color):
    return "#%x%x%x" % color
