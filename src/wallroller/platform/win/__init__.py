import logging
import os
import subprocess
import sys

from wallroller.platform.command import PlatformHooksBase

assert sys.platform == "win32"

NOTIFIER = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "util", "NotifySend.exe"
)

CHANGER = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "util", "SetWallpaper.exe"
)


class PlatformHooks(PlatformHooksBase):
    def __init__(self, manager):
        super().__init__(manager)
        self.manager = manager
        self.config = self.manager.config

    def notify_changing(self):
        pass

    def notify_change(self, selected):
        title = selected.provider if selected.provider else "Wallroller"

        pieces = [f"File: {selected.title}"]

        if selected.source:
            pieces.append(
                "Source: {} {}".format(
                    selected.source,
                    (
                        " (" + selected.source_detail + ")"
                        if selected.source_detail
                        else ""
                    ),
                )
            )

        body = "\n".join(pieces)

        command = [NOTIFIER, selected.path, "-Title", title, "-Body", body]

        logging.info("Notifying wallpaper change with command: %s", command)

        if selected.source:
            command.append("-Source")
            command.append(selected.source)

        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        startupinfo.wShowWindow = subprocess.SW_HIDE

        subprocess.run(command, startupinfo=startupinfo)

    def notify_error(self, err):
        pass

    def set_wallpaper(self, selected):
        command = [CHANGER, selected.path]

        logging.info("Applying wallpaper with command: %s", command)

        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        startupinfo.wShowWindow = subprocess.SW_HIDE

        subprocess.Popen(command, startupinfo=startupinfo)
