param (
    [Parameter(Mandatory)] [String] $Action
)

switch ($Action) {
    "register" {
        $STCommand = (Get-Command pythonw.exe).Source
        [xml]$con = Get-Content .\ScheduledTask.xml

        $con.Task.Principals.Principal.UserId = [System.Security.Principal.WindowsIdentity]::GetCurrent().User.Value
        $con.Task.RegistrationInfo.Author = "$env:ComputerName\$env:UserName"
        $con.Task.Triggers.LogonTrigger.UserId = "$env:ComputerName\$env:UserName"
        $con.Task.Actions.Exec.Command = $STCommand

        $tmpXml = New-TemporaryFile
        $con.Save($tmpXml)

        cat $tmpXml

        schtasks.exe /Create /XML $tmpXml.FullName /TN "Wallroller\CycleWallpaper"
    }
    "unregister" { Unregister-ScheduledTask -TaskPath "\Wallroller\" -TaskName "CycleWallpaper" -Confirm }
    "enable" { Enable-ScheduledTask -TaskPath "\Wallroller\" -TaskName "CycleWallpaper" }
    "disable" { Disable-ScheduledTask -TaskPath "\Wallroller\" -TaskName "CycleWallpaper" }
    "run" { Start-ScheduledTask -TaskPath "\Wallroller\" -TaskName "CycleWallpaper" }
}


