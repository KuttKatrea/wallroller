import logging
import math
import os.path
import sys
import time

from AppKit import (
    NSImageScaleProportionallyUpOrDown,
    NSScreen,
    NSWorkspace,
    NSWorkspaceDesktopImageAllowClippingKey,
    NSWorkspaceDesktopImageFillColorKey,
    NSWorkspaceDesktopImageScalingKey,
)
from Foundation import NSURL, NSColor
from PIL import Image

assert sys.platform == "darwin"


DELTAPROP = 0.10
CENTERPROP = 0.8
TILEPROP = 0.5

PLACEHOLDER_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "../../../graphics/loading.png")
)


def set_wallpaper(filename):
    image = Image.open(filename)
    width, height = image.size

    proportion = width / height

    logging.info(
        "WALLPROVIDER: Image [ Size: %dx%d | Proportion: %f ]",
        width,
        height,
        proportion,
    )

    screen_width, screen_height = get_resolution()
    screen_proportion = screen_width / screen_height

    logging.info(
        "WALLPROVIDER: Screen [ Size: %dx%d | Proportion: %f ]",
        screen_width,
        screen_height,
        screen_proportion,
    )

    proportion_delta = math.fabs(screen_proportion - proportion)

    proportion_perc = proportion_delta / screen_proportion

    logging.info(
        "WALLPROVIDER: Difference: %f / %f %%", proportion_delta, proportion_perc
    )

    color = get_average_color_int(image)

    logging.info("WALLPROVIDER: %s : %s", color, filename)

    do_set_desktop_background(filename, color)


def get_resolution():
    return (
        NSScreen.mainScreen().frame().size.width,
        NSScreen.mainScreen().frame().size.height,
    )


def get_average_color(image):
    return "#%x%x%x" % get_average_color_int(image)


def get_average_color_int(image):
    """Returns a 3-tuple containing the RGB value of the average color of the
    given square bounded area of length = n whose origin (top left corner)
    is (x, y) in the given image"""

    width, height = image.size

    rgb_im = image.convert("RGB")

    r, g, b = 0, 0, 0
    count = 0
    for s in (0, width - 1):
        for t in (0, height - 1):
            pixlr, pixlg, pixlb = rgb_im.getpixel((s, t))
            r += pixlr
            g += pixlg
            b += pixlb
            count += 1

    red = math.floor(r / count) / 256
    green = math.floor(g / count) / 256
    blue = math.floor(b / count) / 256
    return (red, green, blue)


def hexcolor(color):
    return "#%x%x%x" % color


def do_set_desktop_background(filename, color):
    # generate a fileURL for the desktop picture
    file_url = NSURL.fileURLWithPath_(filename)
    dummy_file_url = NSURL.fileURLWithPath_(PLACEHOLDER_PATH)
    # make image options dictionary
    # we just make an empty one because the defaults are fine
    options = {
        NSWorkspaceDesktopImageScalingKey: NSImageScaleProportionallyUpOrDown,
        NSWorkspaceDesktopImageAllowClippingKey: False,
        NSWorkspaceDesktopImageFillColorKey: NSColor.colorWithRed_green_blue_alpha_(
            *color, 1.0
        ),
    }
    # get shared workspace
    ws = NSWorkspace.sharedWorkspace()
    # iterate over all screens
    for screen in NSScreen.screens():
        # tell the workspace to set the desktop picture
        (result, error) = ws.setDesktopImageURL_forScreen_options_error_(
            dummy_file_url, screen, options, None
        )
        logging.info("Dummy Result: %s / %s", result, error)

        time.sleep(0.05)

        (result, error) = ws.setDesktopImageURL_forScreen_options_error_(
            file_url, screen, options, None
        )
        logging.info("Result: %s / %s", result, error)
