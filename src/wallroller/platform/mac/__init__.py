import os
import shutil
import sys

from .. import PlatformHooksBase
from . import notifications, wallprovider

assert sys.platform == "darwin"

SOCKET_NAME = r"/tmp/wallroller/%s/control"
PID_FILE = r"/tmp/wallroller/%s/control"
OUTPUT_PATH = os.path.join(os.path.expanduser("~/Pictures"), "wr-wallpaper")


class PlatformHooks(PlatformHooksBase):
    def __init__(self, manager):
        super().__init__(manager)

    def notify_change(self, selected):
        title = selected.provider if selected.provider else "Wallroller"

        pieces = [f"File: {selected.title}"]

        if selected.source:
            pieces.append(
                "Source: %s %s"
                % (
                    selected.source,
                    (
                        " (" + selected.source_detail + ")"
                        if selected.source_detail
                        else ""
                    ),
                )
            )

        body = "\n".join(pieces)
        icon = selected.path

        notifications.notify(title, body, icon)

    def notify_error(self, err):
        notifications.notify("Wallroller", err, "nitrogen")

    def notify_changing(self):
        # notifications.notify("Wallroller", "Selecting next wallpaper")
        pass

    def set_wallpaper(self, selected):
        (_, ext) = os.path.splitext(selected.path)
        output_path = OUTPUT_PATH + ext

        shutil.copy(selected.path, output_path)
        wallprovider.set_wallpaper(output_path)
