from AppKit import NSImage, NSObject, NSUserNotification, NSUserNotificationCenter


class NotificationDelegator(NSObject):
    def userNotificationCenter_didActivateNotification_(self, _center, _notification):
        print("user notification center")

    def userNotificationCenter_shouldPresentNotification_(self, _center, _notification):
        return True


delegator = NotificationDelegator.alloc().init()
notificationCenter = NSUserNotificationCenter.defaultUserNotificationCenter()
notificationCenter.setDelegate_(delegator)


def notify(title, body, icon):
    notification = NSUserNotification.alloc().init()
    # notification.setIdentifier = "unique-id"
    notification.setTitle_(title)
    # notification.setSubtitle_()
    notification.setInformativeText_(body)
    # notification.setSoundName_("NSUserNotificationDefaultSoundName")
    notification.setContentImage_(NSImage.alloc().initByReferencingFile_(icon))

    # notification.set_showsButtons_(True)
    # notification.setHasActionButton_(True)
    # notification.setActionButtonTitle_("View")
    # notification.setUserInfo_({"action": "open_url", "value": icon})

    notificationCenter.scheduleNotification_(notification)
