import enum
from typing import Protocol

from wallroller.config import Config
from wallroller.image import Image
from wallroller.providers import ProviderSelector


class Command(str, enum.Enum):
    NOTIFY = "notify"
    NEXT = "next"
    QUIT = "quit"
    APPLY = "apply"


class ManagerProtocol(Protocol):
    config: Config

    def __init__(self, config: Config, providers: ProviderSelector):
        pass


class PlatformHooksBase(Protocol):
    def __init__(self, _: ManagerProtocol):
        pass

    def notify_changing(self):
        pass

    def notify_change(self, selected: Image):
        pass

    def notify_error(self, err: str):
        pass

    def set_wallpaper(self, selected: Image):
        pass
