import logging

from wallroller.config import Config, SelectionOptions
from wallroller.image import Image
from wallroller.lib import exception
from wallroller.lib.util import RetryCounter
from wallroller.platform import create_platform_hooks
from wallroller.platform.command import ManagerProtocol
from wallroller.providers import ProviderSelector


class Manager(ManagerProtocol):
    def __init__(self, config: Config, providers: ProviderSelector):
        super().__init__(config, providers)
        self.config = config
        self.current: Image | None = None
        self.providers = providers

        self.platform = create_platform_hooks(self)
        self.selected: Image | None = None

    def wallpaper_change(self, options: SelectionOptions):
        self.platform.notify_changing()
        retries = RetryCounter(
            options.retries if options.retries is not None else self.config.retries
        )
        while retries.has_available():
            try:
                self.do_wallpaper_change(retries, options)
                break
            except Exception as ex:
                if isinstance(ex, exception.ControlledException):
                    logging.warning(ex)
                else:
                    logging.exception(ex)
                retries.consume()
                logging.info("Retries left: %s", retries.available())

        logging.debug("Number of retries: %s", retries.consumed())

    def do_wallpaper_change(self, retries: RetryCounter, options: SelectionOptions):
        selected_image = self.providers.select(retries, options)

        if selected_image is None:
            raise exception.NoItemException("Valid image not found")

        logging.info("Selected image: %s", selected_image.source)

        if options.new_only and not selected_image.is_downloaded:
            raise exception.ItemNotNewException("The image is not new")

        if not options.download_only:
            self.wallpaper_apply(selected_image)

    def wallpaper_apply(self, selected_image: Image):
        self.selected = selected_image

        self.platform.set_wallpaper(selected_image)

        self.update_current(selected_image)

        self.notify_current()

    def update_current(self, selected: Image):
        self.current = selected

    def notify_current(self):
        if self.current:
            self.platform.notify_change(self.current)

    def notify_error(self, err):
        self.platform.notify_error(err)

    def reapply(self):
        if self.current:
            self.platform.set_wallpaper(self.current)
            self.notify_current()
