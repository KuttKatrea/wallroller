from wallroller import config, manager
from wallroller.providers import ProviderSelector


def build(profile: str):
    config_file = config.get_profile_config_file(profile)
    profile_config = config.configure_for(profile, config_file)
    profile_config.newonly = False

    providers = ProviderSelector(profile_config)

    man = manager.Manager(profile_config, providers)

    return man
