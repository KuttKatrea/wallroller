import os.path
from typing import Mapping

from wallroller.config import Config, SelectionOptions

from .. import image
from ..lib.exception import NoItemException


def create_metafile(target_path: str, source_url: str, meta: Mapping | None):
    target_dir = os.path.dirname(target_path)
    os.makedirs(target_dir, exist_ok=True)

    with open(target_path, "w", encoding="utf-8") as fp:
        content = ["[InternetShortcut]\n", f"URL={source_url}\n"]

        if meta:
            content.append("[Wallroller]\n")
            for key, val in meta.items():
                content.append(f"{key}={val}\n")

        fp.writelines(content)


class BaseProvider:
    def __init__(self, key: int, config: Config, provider_config: dict):
        self.key = key
        self.engine_id: str = provider_config["engine"]
        self.enabled: bool = (
            provider_config["enabled"] if "enabled" in provider_config else True
        )
        self.config = provider_config

        self.data_dir: str = config.get_provider_config_path(self.engine_id)
        self.profile_data_dir: str = config.get_provider_profile_config_path(
            self.engine_id
        )

        self.provider_config = provider_config

        self.name = self.engine_id
        self._init_provider()

    def _init_provider(self) -> None:
        pass

    def select(self, options: SelectionOptions) -> image.Image:
        raise NoItemException()
