import logging

from wallroller.lib.exception import BackoffException, InvalidProviderError

from ..config import Config, SelectionOptions
from ..image import Image
from ..lib.util import RetryCounter, secrets
from .base import BaseProvider
from .danbooru import DanbooruProvider, TestbooruProvider
from .filesystem import FileSystemProvider
from .gelbooru import GelbooruProvider
from .konachan import KonachanNetProvider, KonachanProvider
from .pixiv import PixivProvider
from .sankakubeta import SankakuBetaProvider
from .threedeebooru import ThreeDeebooruProvider
from .yandere import YandereProvider

engine_map_ = {
    "filesystem": FileSystemProvider,
    "konachan": KonachanProvider,
    "konachan-net": KonachanNetProvider,
    "yandere": YandereProvider,
    "danbooru": DanbooruProvider,
    "danbooru-test": TestbooruProvider,
    "sankakubeta": SankakuBetaProvider,
    "3dbooru": ThreeDeebooruProvider,
    "gelbooru": GelbooruProvider,
    "pixiv": PixivProvider,
}


def create_engine(key: int, config: Config, provider_config: dict) -> BaseProvider:
    engine_id = provider_config["engine"]
    logging.debug(f"Creating engine for {key} => {engine_id}")

    try:
        cls = engine_map_[engine_id]
    except KeyError as err:
        raise InvalidProviderError(f"No provider called {engine_id}") from err

    return cls(key=key, config=config, provider_config=provider_config)


def get_providers(config: Config) -> dict[str, BaseProvider]:
    providers_ = {}

    for idx, single_config in config.providers.items():
        logging.debug(f"Configuring provider: {idx}")

        if not single_config.get("enabled", True):
            logging.info("Disabled")
            continue

        try:
            engine = create_engine(
                key=idx, config=config, provider_config=single_config
            )
        except InvalidProviderError as err:
            logging.warning("Invalid provider", exc_info=err)
            continue

        providers_[idx] = engine

    if len(providers_) <= 0:
        raise Exception("No hay proveedores configurados")

    return providers_


class ProviderSelector:
    def __init__(self, config: Config):
        self.providers_ = get_providers(config)

    def select(self, retries: RetryCounter, options: SelectionOptions) -> Image | None:
        if options.selected_providers:
            available_providers = {
                key: val
                for key, val in self.providers_.items()
                if key in options.selected_providers
            }
        else:
            available_providers = self.providers_

        while retries.has_available():
            if len(available_providers) == 0:
                raise Exception("No providers available")

            selected_provider_key = secrets.choice(list(available_providers.keys()))
            selected_provider = available_providers[selected_provider_key]

            logging.info(
                "Selected provider: %s (%s)",
                selected_provider_key,
                selected_provider.engine_id,
            )
            try:
                return selected_provider.select(options)
            except BackoffException as ex:
                logging.debug("Backoff Exception received", exc_info=ex)
                logging.info(
                    "%s requested a back-off: %s", selected_provider.engine_id, ex
                )
                available_providers = {
                    key: val
                    for key, val in available_providers.items()
                    if val.engine_id != selected_provider.engine_id
                }
                retries.consume()

        return None
