from wallroller.providers.moebooru_api import MoebooruApiBaseProvider, MoebooruApiConfig

KONACHAN_MOEBOORU_CONFIG = MoebooruApiConfig(
    provider_name="Konachan",
    api_base_url="https://konachan.com/post.xml",
    source_base_url="https://konachan.com/post/show/{post_id}",
)

KONACHAN_NET_MOEBOORU_CONFIG = MoebooruApiConfig(
    provider_name="Konachan.net",
    api_base_url="https://konachan.net/post.xml",
    source_base_url="https://konachan.net/post/show/{post_id}",
)


class KonachanProvider(MoebooruApiBaseProvider):
    moebooru_config = KONACHAN_MOEBOORU_CONFIG


class KonachanNetProvider(MoebooruApiBaseProvider):
    moebooru_config = KONACHAN_NET_MOEBOORU_CONFIG
