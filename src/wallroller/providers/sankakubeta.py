import base64
import json
import logging
import os
import os.path
import re
import secrets
import urllib.parse
from datetime import datetime

import requests
import yaml

from wallroller.config import SelectionOptions

from ..lib.booru import BooruCategoryGroup, EmptyGalleryException
from .booru import BooruBaseProvider, BooruPostImage

API_BASE_URL = "https://capi-v2.sankakucomplex.com"

USERAGENT = (
    "Mozilla/5.0 "
    "(Windows NT 10.0; Win64; x64; rv:93.0) "
    "Gecko/20100101 Firefox/93.0"
)

REFERER = "https://beta.sankakucomplex.com/"

BANNED_CONTENT_TYPES = frozenset(["video/mp4"])


def create_post_url(post_id):
    return f"https://beta.sankakucomplex.com/post/show/{post_id}"


class SankakuBetaProvider(BooruBaseProvider):
    def _init_provider(self):
        super()._init_provider()
        self.data_file_path = os.path.join(self.data_dir, "auth.yaml")
        if os.path.exists(self.data_file_path):
            with open(self.data_file_path, mode="r", encoding="utf-8") as fptr:
                self.auth_data = yaml.load(fptr, Loader=yaml.SafeLoader)
        else:
            self.auth_data = {
                "access_token": None,
                "refresh_token": None,
            }

    def refresh_session(self):
        if self.auth_data["access_token"]:
            match = re.search(r"\.(.*)\.", self.auth_data["access_token"])
            if match:
                payload_b64 = match.group(1)
                payload = json.loads(base64.b64decode(payload_b64 + "=="))
                exp = datetime.fromtimestamp(payload["exp"])
                now = datetime.now()
                if now < exp:
                    return

        response = requests.post(
            f"{API_BASE_URL}/auth/token?lang=en",
            headers={
                "user-agent": USERAGENT,
                "referer": REFERER,
                "accept": "application/vnd.sankaku.api+json;v=2",
                "accept-language": "en-US,en,es-MX",
                "content-type": "application/json",
                "client-type": "non-premium",
                "origin": REFERER,
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "cross-site",
                "connection": "keep-alive",
                "te": "trailers",
            },
            json={"refresh_token": self.auth_data["refresh_token"]},
        )

        response.raise_for_status()

        response_data = response.json()
        self.auth_data["access_token"] = response_data["access_token"]
        self.auth_data["refresh_token"] = response_data["refresh_token"]

        with open(self.data_file_path, mode="w", encoding="utf-8") as fptr:
            yaml.dump(self.auth_data, stream=fptr)

    def get_post(
        self, selected_group: BooruCategoryGroup, options: SelectionOptions
    ) -> BooruPostImage:
        self.refresh_session()

        response = requests.get(
            f"{API_BASE_URL}/posts/keyset",
            params={
                "lang": "en",
                "default_threshold": "1",
                "hide_posts_in_books": "in-larger-tags",
                "limit": "40",
                "tags": selected_group.tags,
            },
            headers={
                "user-agent": USERAGENT,
                "accept": "application/vnd.sankaku.api+json;v=2",
                "accept-language": "en-US,en",
                "origin": REFERER,
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-site",
                "authorization": f"Bearer {self.auth_data['access_token']}",
                "referer": REFERER,
                "Api-Version": "2",
                "Client-Type": "non-premium",
                "Platform": "web-app",
            },
        )

        response_data = response.json()

        logging.debug(response_data)

        if "data" not in response_data:
            logging.error(response_data)
            raise EmptyGalleryException

        filtered_data = [
            it
            for it in response_data["data"]
            if it.get("file_type") not in BANNED_CONTENT_TYPES
        ]

        logging.debug(filtered_data)

        if len(filtered_data) == 0:
            raise EmptyGalleryException

        selected_item = secrets.choice(filtered_data)
        logging.debug(json.dumps(selected_item))

        image_url = selected_item["file_url"]

        logging.info("Image URL: %s", image_url)
        logging.info("Post tags: %s", [tag["name"] for tag in selected_item["tags"]])

        image_name = os.path.basename(urllib.parse.urlsplit(image_url).path)
        image_subdir = image_name[0:3]

        logging.debug("Image name: %s", image_name)
        image_filename = os.path.normpath(
            os.path.join(self.config["path"], image_subdir, image_name)
        )
        os.makedirs(os.path.dirname(image_filename), exist_ok=True)

        post_url = create_post_url(selected_item["id"])

        logging.debug("Image filename: %s", image_filename)

        return BooruPostImage(
            image_name,
            image_url,
            image_filename,
            post_url,
            meta={
                "tags": " ".join(tag["name"] for tag in selected_item.get("tags", [])),
                "rating": selected_item.get("rating"),
            },
        )
