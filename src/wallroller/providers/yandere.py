from wallroller.providers.moebooru_api import MoebooruApiBaseProvider, MoebooruApiConfig

YANDERE_MOEBOORU_CONFIG = MoebooruApiConfig(
    provider_name="Yande.re",
    api_base_url="https://yande.re/post.xml",
    source_base_url="https://yande.re/post/show/{post_id}",
)


class YandereProvider(MoebooruApiBaseProvider):
    moebooru_config = YANDERE_MOEBOORU_CONFIG
