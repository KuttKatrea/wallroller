import abc
import logging
import secrets
from typing import Mapping, NamedTuple

from wallroller import downloader
from wallroller.config import SelectionOptions
from wallroller.image import Image
from wallroller.lib import exception
from wallroller.lib.booru import BooruCategoryGroup, group_hint_matcher
from wallroller.lib.util import flatten
from wallroller.providers.base import BaseProvider


class BooruPostImage(NamedTuple):
    image_name: str
    image_url: str
    image_filename: str
    source_url: str
    referer: str | None = None
    meta: Mapping[str, str] | None = None


class BooruBaseProvider(BaseProvider):
    def _init_provider(self):
        self.name = self.engine_id
        if self.provider_config.get("groups", None) is None:
            self.provider_config["groups"] = [""]

        groups = flatten(self.provider_config["groups"])
        self.path = self.provider_config["path"]
        self.common_tags = self.provider_config.get("tags", "rating:safe")

        self.groups = [
            BooruCategoryGroup(" ".join((self.common_tags, group))) for group in groups
        ]

    def get_default_request_parameters(self, request_type: str | None = None):
        return {}

    def select(self, options: SelectionOptions):
        if options.hint:
            if options.hint.startswith("query:"):
                selectable_groups = [BooruCategoryGroup(options.hint[6:])]
            else:
                selectable_groups = list(
                    filter(group_hint_matcher(options.hint), self.groups)
                )
        else:
            selectable_groups = self.groups

        if not selectable_groups:
            raise exception.NoItemException("No valid groups available.")

        selected_group = secrets.choice(selectable_groups)

        if selected_group is None:
            raise exception.NoItemException("Invalid group selected.")

        logging.info("Selected group tags: %s", selected_group.tags)

        post_image = self.get_post(selected_group, options)

        if post_image.image_url is None:
            raise exception.InvalidItemException(
                "Item doesn't have a URL. Maybe was deleted."
            )

        logging.info("Selected post: %s", post_image.source_url)

        request_parameters = self.get_default_request_parameters("download")
        request_parameters.setdefault("headers", {})

        if post_image.referer is not None:
            request_parameters["headers"].setdefault("referer", post_image.referer)

        result = downloader.download(
            post_image.image_url,
            post_image.image_filename,
            post_image.source_url,
            request_parameters=request_parameters,
            meta=post_image.meta,
        )

        return Image(
            title=post_image.image_name,
            source=post_image.source_url,
            path=post_image.image_filename,
            provider=f"{self.engine_id} ({selected_group.tags})",
            is_downloaded=result.is_downloaded,
        )

    @abc.abstractmethod
    def get_post(
        self, selected_group: BooruCategoryGroup, options: SelectionOptions
    ) -> BooruPostImage:
        pass
