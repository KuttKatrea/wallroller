import logging
import os
import urllib.parse
from dataclasses import dataclass
from io import BytesIO

import requests
from lxml import etree

from wallroller.config import SelectionOptions
from wallroller.lib.booru import BooruCategoryGroup, EmptyGalleryException, get_cache
from wallroller.lib.util import secrets
from wallroller.providers.booru import BooruBaseProvider, BooruPostImage

USERAGENT = (
    "Mozilla/5.0 "
    "(Windows NT 10.0; Win64; x64; rv:93.0) "
    "Gecko/20100101 Firefox/93.0"
)


@dataclass
class MoebooruApiConfig:
    provider_name: str
    api_base_url: str
    source_base_url: str
    max_search_page: int | None = None


def get_image_name(post_data):
    basename = post_data.get("md5")

    pieces = urllib.parse.urlparse(post_data.get("file_url"))
    (_, ext) = os.path.splitext(pieces.path)
    return f"{basename}{ext}"


def get_image_subdirectory(post_data):
    image_base_name = post_data.get("md5")
    return f"{image_base_name[0:3]}"


class MoebooruApiBaseProvider(BooruBaseProvider):
    moebooru_config: MoebooruApiConfig

    def _init_provider(self):
        super()._init_provider()
        self._api_key = self.provider_config.get("api_key")
        self._user_id = self.provider_config.get("user_id")

    def get_post(self, selected_group: BooruCategoryGroup, options: SelectionOptions):
        booru_cache = get_cache(self.data_dir)
        group_max_page = booru_cache.get_maxpage(
            self.moebooru_config.api_base_url, selected_group.tags
        )

        if self.moebooru_config.max_search_page is not None:
            if group_max_page > self.moebooru_config.max_search_page:
                group_max_page = self.moebooru_config.max_search_page

        max_search_page = group_max_page

        if not options.known_pages_only:
            # Some engines don's return the max number of pages, so we consider one extra page to test.
            # But if we only want to query the already known existing pages (qhen searching by ID, for example)
            # We don't need this behaviour
            group_max_page += 1

        page = secrets.randint(1, max_search_page)
        request_params = {"page": str(page), "tags": selected_group.tags, "limit": "1"}

        if self._user_id and self._api_key:
            request_params["api_key"] = self._api_key
            request_params["user_id"] = self._user_id

        response = requests.get(
            self.moebooru_config.api_base_url,
            params=request_params,
            headers={
                "user-agent": USERAGENT,
            },
        )
        response.raise_for_status()

        parser = etree.XMLParser()
        doc = etree.parse(BytesIO(response.content), parser)
        root = doc.getroot()

        logging.debug(etree.tostring(root))

        post_data = root.find("post")

        # Calculates max page number
        api_page_count = int(root.get("count", 0))
        if post_data is not None:
            current_page_count = page
        else:
            current_page_count = 0

        new_total = max(api_page_count, current_page_count, group_max_page)

        if new_total > group_max_page:
            booru_cache.update_maxpage(
                self.moebooru_config.api_base_url, selected_group.tags, new_total
            )

        # End of calculates max page number
        if post_data is None:
            raise EmptyGalleryException("Empty gallery page")

        post_id = post_data.get("id")
        logging.info("Post selected: %s", post_id)
        logging.info("Post tags: %s", post_data.get("tags"))

        image_url = post_data.get("file_url")
        source_url = self.moebooru_config.source_base_url.format(post_id=post_id)
        image_name = get_image_name(post_data)
        image_filename = os.path.abspath(
            os.path.join(self.path, get_image_subdirectory(post_data), image_name)
        )

        return BooruPostImage(
            image_name,
            image_url,
            image_filename,
            source_url,
            meta={"tags": post_data.get("tags"), "rating": post_data.get("rating")},
        )
