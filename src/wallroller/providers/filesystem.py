import logging
import os
import os.path
import re
from collections import namedtuple

from .. import image
from ..lib import exception
from ..lib.util import secrets
from . import base

DirectoryLeaf = namedtuple("DirectoryLeaf", ["root_path", "path"])


class FileSystemProvider(base.BaseProvider):
    def _init_provider(self) -> None:
        self.directories = self.provider_config["directories"]

    def select(self, hint):
        if hint:
            raise exception.NoItemException("No hint support.")

        directories: list[DirectoryLeaf] = []

        for single_dir in self.directories:
            path = os.path.realpath(single_dir["path"])
            path_name = os.path.basename(path)
            try:
                recursive = single_dir["recursive"]
            except KeyError:
                recursive = False

            if recursive:
                for single_path in os.walk(path):
                    directories.append(
                        DirectoryLeaf(path_name, os.path.realpath(single_path[0]))
                    )
            else:
                directories.append(DirectoryLeaf(path_name, path))

        if not directories:
            raise exception.NoItemException("There are no directories to choose from.")

        selected_directory = secrets.choice(directories)

        logging.info("Selected directory: %s", selected_directory.root_path)

        files = []

        for file in os.listdir(selected_directory.path):
            if re.search(r"\.(jpg|jpeg|gif|png)$", file):
                files.append(file)

        if len(files) <= 0:
            raise exception.NoItemException("No valid items available.")

        image_name = secrets.choice(files)

        image_path = os.path.realpath(os.path.join(selected_directory.path, image_name))

        return image.Image(
            title=image_name,
            source=image_path,
            path=image_path,
            provider=f"Local / {os.path.basename(selected_directory.root_path)}",
            is_downloaded=True,
        )
