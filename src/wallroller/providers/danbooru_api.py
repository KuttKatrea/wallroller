import logging
import math
import os
from dataclasses import dataclass
from io import BytesIO

import requests
from lxml import etree

from wallroller.config import SelectionOptions
from wallroller.lib.booru import EmptyGalleryException, get_cache
from wallroller.lib.exception import InvalidItemException
from wallroller.lib.util import secrets
from wallroller.providers.booru import BooruBaseProvider, BooruPostImage

DANBOORU_PAGE_SIZE = 100

BANNED_FILE_EXT = frozenset(["mp4"])


@dataclass
class DanbooruApiConfig:
    provider_name: str
    api_base_url: str
    source_base_url: str
    max_search_page: int | None = None


def get_image_name(post_data):
    basename = post_data.find("md5").text
    ext = post_data.find("file-ext").text
    return f"{basename}.{ext}"


def get_image_subdirectory(post_data):
    image_base_name = post_data.find("md5").text
    return f"{image_base_name[0:3]}"


class DanbooruApiBaseProvider(BooruBaseProvider):
    booru_api_config: DanbooruApiConfig

    def _init_provider(self) -> None:
        super()._init_provider()

        if "api_key" not in self.provider_config:
            raise Exception(f"api_key is needed for {self.name} provider.")

        if "user_id" not in self.provider_config:
            raise Exception(f"user_id is needed for {self.name} provider.")

        self._api_key: str = self.provider_config["api_key"]
        self._user_id: str = self.provider_config["user_id"]

    def get_default_request_parameters(self, request_type: str | None = None):
        return {
            "headers": {
                "user-agent": self._user_id,
            },
            "auth": (self._user_id, self._api_key),
        }

    def get_post(self, selected_group, options: SelectionOptions) -> BooruPostImage:
        booru_cache = get_cache(self.data_dir)
        group_max_works = booru_cache.get_maxpage(
            self.booru_api_config.api_base_url, selected_group.tags
        )

        max_search_page = math.ceil(group_max_works / DANBOORU_PAGE_SIZE)

        if not options.known_pages_only:
            max_search_page += 1

        page = secrets.randint(1, max_search_page)
        logging.info(
            "Selected page: %s (%s out of ~%s known works)",
            page,
            DANBOORU_PAGE_SIZE,
            group_max_works,
        )
        request_params = {
            "page": page,
            "tags": selected_group.tags,
            "limit": DANBOORU_PAGE_SIZE,
        }

        response = requests.get(
            self.booru_api_config.api_base_url,
            params=request_params,
            **self.get_default_request_parameters(),
        )
        response.raise_for_status()

        parser = etree.XMLParser()
        doc = etree.parse(BytesIO(response.content), parser)
        root = doc.getroot()

        logging.debug("Received XML: %s", etree.tostring(root))

        post_list = list(root.iter("post"))

        logging.debug("Post list size: %s", len(post_list))

        # Calculates max page number
        if len(post_list) > 0:
            current_page_count = page * DANBOORU_PAGE_SIZE
        else:
            current_page_count = 0

        new_total = max(current_page_count, group_max_works)

        if new_total > group_max_works:
            booru_cache.update_maxpage(
                self.booru_api_config.api_base_url, selected_group.tags, new_total
            )

        if len(post_list) == 0:
            raise EmptyGalleryException("Empty gallery page")

        post_data = secrets.choice(post_list)

        logging.debug("Post XML: %s", etree.tostring(post_data))

        selected_post_id = post_data.find("id").text
        logging.info("Selected post ID: %s", selected_post_id)
        logging.info(
            "Selected post tags (general): %s",
            post_data.find("tag-string-general").text,
        )
        logging.info(
            "Selected post tags (character): %s",
            post_data.find("tag-string-character").text,
        )
        logging.info(
            "Selected post tags (copyright): %s",
            post_data.find("tag-string-copyright").text,
        )
        logging.info(
            "Selected post tags (artist): %s", post_data.find("tag-string-artist").text
        )
        logging.info(
            "Selected post tags (meta): %s", post_data.find("tag-string-meta").text
        )

        file_ext = post_data.find("file-ext").text

        if file_ext in BANNED_FILE_EXT:
            raise Exception(f"Banned file type: {file_ext}")

        file_url_element = post_data.find("file-url")

        if file_url_element is None:
            raise InvalidItemException(
                "Item doesn't have file_url. Most likely is a banned post."
            )

        image_url = file_url_element.text
        source_url = self.booru_api_config.source_base_url.format(
            post_id=selected_post_id
        )
        image_name = get_image_name(post_data)

        logging.info("Image URL: %s", image_url)

        if image_url is None:
            raise Exception("Cannot find the image url")

        image_filename = os.path.abspath(
            os.path.join(self.path, get_image_subdirectory(post_data), image_name)
        )

        return BooruPostImage(
            image_name=image_name,
            image_url=image_url,
            image_filename=image_filename,
            source_url=source_url,
            meta={
                "tag-string-general": post_data.find("tag-string-general").text,
                "tag-string-character": post_data.find("tag-string-character").text,
                "tag-string-copyright": post_data.find("tag-string-copyright").text,
                "tag-string-artist": post_data.find("tag-string-artist").text,
                "tag-string-meta": post_data.find("tag-string-meta").text,
                "rating": post_data.find("rating").text,
            },
        )
