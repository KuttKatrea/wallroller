import logging
import os
import urllib.parse

import requests
import yaml

from wallroller.config import SelectionOptions
from wallroller.lib.booru import BooruCategoryGroup, EmptyGalleryException, get_cache
from wallroller.lib.exception import BackoffException
from wallroller.lib.util import secrets
from wallroller.providers.booru import BooruBaseProvider, BooruPostImage

PROVIDER_NAME = "Gelbooru"
BASE_API_URL = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags={tags}&pid={page}&limit=1&json=1"
SOURCE_BASE_URL = "https://gelbooru.com/index.php?page=post&s=view&id={post_id}"
REFERER = "https://gelbooru.com/"


def get_image_name(post_data):
    return post_data["image"]


def get_image_subdirectory(post_data):
    return post_data["image"][0:3]


class GelbooruProvider(BooruBaseProvider):
    def _init_provider(self):
        super()._init_provider()
        self.data_file_path = os.path.join(self.data_dir, "auth.yaml")
        if os.path.exists(self.data_file_path):
            with open(self.data_file_path, mode="r", encoding="utf-8") as fptr:
                self.auth_data = yaml.load(fptr, Loader=yaml.SafeLoader)
        else:
            self.auth_data = {}

        self.api_key = self.auth_data.get("api_key")
        self.user_id = self.auth_data.get("user_id")

    def get_post(self, selected_group: BooruCategoryGroup, options: SelectionOptions):
        booru_cache = get_cache(self.data_dir)
        group_max_page = booru_cache.get_maxpage(BASE_API_URL, selected_group.tags) or 1

        page = secrets.randint(0, group_max_page - 1)
        url = BASE_API_URL.format(
            page=page, tags=urllib.parse.quote(selected_group.tags)
        )

        if self.user_id and self.api_key:
            url += f"&api_key={self.api_key}&user_id={self.user_id}"

        logging.info(url)

        response = requests.get(url)
        response.raise_for_status()

        try:
            data = response.json()
        except requests.exceptions.JSONDecodeError as ex:
            raise BackoffException(response.text) from ex

        logging.debug(data)
        total = data["@attributes"]["count"]

        if total == 0:
            raise EmptyGalleryException("Empty gallery page")

        booru_cache.update_maxpage(BASE_API_URL, selected_group.tags, total)

        posts = data.get("post")

        if not posts:
            raise EmptyGalleryException("Empty gallery page")

        post_data = data["post"][0]

        image_url = post_data["file_url"]
        source_url = SOURCE_BASE_URL.format(post_id=post_data["id"])
        image_name = get_image_name(post_data)

        logging.info("Post tags: %s", post_data["tags"])
        logging.info("Image URL: %s", image_url)

        if image_url is None:
            raise Exception("Cannot find the image url")

        image_filename = os.path.join(
            self.path, get_image_subdirectory(post_data), image_name
        )

        return BooruPostImage(
            image_name,
            image_url,
            image_filename,
            source_url,
            meta={"tags": post_data["tags"], "rating": post_data.get("rating")},
        )
