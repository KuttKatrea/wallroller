import json
import logging
import os.path
import posixpath
import urllib.parse

import yaml
from pixivpy3 import AppPixivAPI

from wallroller.config import SelectionOptions

from .. import image
from ..lib import exception
from ..lib.util import flatten, secrets
from . import base
from .base import create_metafile


class PixivProvider(base.BaseProvider):
    def _init_provider(self):
        self.path = self.config["path"]
        self.client = AppPixivAPI()
        self.logged_in = False
        self.limit_per_group = {}
        self.default_tags = self.config.get("default_tags", [])
        self.banned_tags = frozenset(self.config.get("banned_tags", {"R-18"}))
        self.groups = flatten(self.config.get("groups", []))

        self.data_file_path = os.path.join(self.data_dir, "auth.yaml")
        if os.path.exists(self.data_file_path):
            with open(self.data_file_path, mode="r", encoding="utf-8") as fptr:
                self.auth_data = yaml.load(fptr, Loader=yaml.SafeLoader)
        else:
            self.auth_data = {}

        logging.debug("Default Tags: %s", self.default_tags)
        logging.debug("Banned Tags: %s", self.banned_tags)
        logging.debug("Groups: %s", self.groups)

    def select(self, options: SelectionOptions):
        if not self.logged_in:
            self.client.auth(refresh_token=self.auth_data.get("refresh_token"))
            self.logged_in = True

        if options.hint and options.hint.startswith("id:"):
            return self.select_by_id(options.hint[3:])

        if self.config.get("mode") == "bookmarks":
            return self.select_bookmarks(options)

        if self.config.get("mode") == "follows":
            return self.select_follow(options)

        return self.select_groups(options)

    def select_bookmarks(self, options: SelectionOptions):
        if not self.logged_in:
            self.client.auth(refresh_token=self.auth_data.get("refresh_token"))
            self.logged_in = True

        if options.hint:
            raise exception.NoItemException("No hint support.")

        works = self.client.user_bookmarks_illust(
            self.client.user_id,
            restrict=self.config.get("restrict", "public"),
            req_auth=True,
        )

        logging.debug("All works: %s", json.dumps(works, indent=2))

        return self.select_from_works(works, "User Bookmarks")

    def select_follow(self, options: SelectionOptions):
        if not self.logged_in:
            self.client.auth(refresh_token=self.auth_data.get("refresh_token"))
            self.logged_in = True

        if options.hint:
            raise exception.NoItemException("No hint support.")

        works = self.client.illust_follow(
            restrict=self.config.get("restrict", "public"), req_auth=True
        )

        logging.debug("All works: %s", json.dumps(works, indent=2))

        return self.select_from_works(works, "User Following")

    def select_groups(self, options: SelectionOptions):
        if not self.logged_in:
            self.client.auth(refresh_token=self.auth_data.get("refresh_token"))
            self.logged_in = True

        if options.hint:
            selectable_groups = [g for g in self.groups if options.hint in g]
        else:
            selectable_groups = self.groups

        if not selectable_groups:
            raise exception.NoItemException("No valid groups available.")

        group = secrets.choice(selectable_groups)

        logging.info("Group selected: %s", group)

        works = self.client.search_illust(
            word=" ".join(self.default_tags + [group]),
            search_target="partial_match_for_tags",
            sort="date_desc",
        )

        logging.debug("All works: %s", json.dumps(works))

        return self.select_from_works(works, group)

    def select_from_works(self, works, group):
        response = [
            item
            for item in works["illusts"]
            if not has_banned_tags(item.tags, self.banned_tags)
        ]

        if len(response) == 0:
            raise exception.NoItemException("No valid items available.")

        work = secrets.choice(response)
        return self.use_work(work, group)

    def use_work(self, work, group):
        logging.info("Work selected: %s, %s pages", work.title, work.page_count)
        logging.info("Tags: %s", ", ".join(map(lambda m: m.name, work.tags)))
        logging.debug("Full json: %s", json.dumps(work))

        source_url = f"https://www.pixiv.net/en/artworks/{work.id}"

        meta = {
            "title": work.get("title"),
            "type": work.get("type"),
            "user_id": work.get("user", {}).get("id"),
            "user_name": work.get("user", {}).get("name"),
            "tags": " ".join(source_tag.name for source_tag in work.tags),
            "sanity_level": str(work.get("sanity_level")),
        }

        if work.page_count == 1:
            _source_url, image_filename, downloaded = self.download(
                work.meta_single_page.original_image_url, source_url, meta=meta
            )
        else:
            work_pages = [
                self.download(meta_page.image_urls.original, source_url, meta=meta)
                for meta_page in work.meta_pages
            ]
            _source_url, image_filename, downloaded = secrets.choice(work_pages)

        return image.Image(
            title=work.title,
            source=source_url,
            path=image_filename,
            provider=f"Pixiv / {group}",
            is_downloaded=downloaded,
        )

    def select_by_id(self, illust_id: str):
        illust_detail = self.client.illust_detail(illust_id)

        if "error" in illust_detail:
            raise exception.InvalidItemException(
                illust_detail["error"].get("user_message", "Error")
            )

        return self.use_work(illust_detail["illust"], illust_id)

    def download(self, download_url, source_url, meta):
        image_path = urllib.parse.urlsplit(download_url).path
        image_name = urllib.parse.unquote_plus(posixpath.basename(image_path))
        image_subdir = image_name[0:3]
        image_save_path = os.path.realpath(os.path.join(self.path, image_subdir))
        image_filename = os.path.join(image_save_path, image_name)

        downloaded = False

        create_metafile(
            target_path=image_filename + ".url", source_url=source_url, meta=meta
        )

        if not os.path.exists(image_filename):
            logging.debug("Ensure dirs %s exists", image_save_path)
            os.makedirs(image_save_path, exist_ok=True)

            logging.info("Downloading %s to %s", download_url, image_filename)

            with open(image_filename, "wb") as fp:
                self.client.download(url=download_url, fname=fp)

            downloaded = True
        else:
            logging.info("%s already exists.", image_filename)

        return download_url, image_filename, downloaded


def has_banned_tags(source_tags, banned_tags):
    for source_tag in source_tags:
        if source_tag.name in banned_tags:
            return True

    return False
