from .danbooru_api import DanbooruApiBaseProvider, DanbooruApiConfig

DANBOORU_API_CONFIG = DanbooruApiConfig(
    provider_name="Danbooru",
    api_base_url="https://danbooru.donmai.us/posts.xml",
    source_base_url="https://danbooru.donmai.us/posts/{post_id}",
)


class DanbooruProvider(DanbooruApiBaseProvider):
    booru_api_config = DANBOORU_API_CONFIG


TESTBOORU_API_CONFIG = DanbooruApiConfig(
    provider_name="Testbooru",
    api_base_url="https://testbooru.donmai.us/posts.xml",
    source_base_url="https://testbooru.donmai.us/posts/{post_id}",
)


class TestbooruProvider(DanbooruApiBaseProvider):
    booru_api_config = TESTBOORU_API_CONFIG
