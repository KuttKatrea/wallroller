from wallroller.providers.moebooru_api import MoebooruApiBaseProvider, MoebooruApiConfig

THREEDEEBOORU_MOEBOORU_CONFIG = MoebooruApiConfig(
    provider_name="3Dbooru",
    api_base_url="http://behoimi.org/post/index.xml",
    source_base_url="http://behoimi.org/post/show/{post_id}",
    max_search_page=1000,
)


class ThreeDeebooruProvider(MoebooruApiBaseProvider):
    moebooru_config = THREEDEEBOORU_MOEBOORU_CONFIG
