from wallroller.config import Config


class Local(object):
    def __init__(self, config: Config):
        self.config = config

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, value):
        self._current = value
